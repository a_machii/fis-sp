var gulp = require("gulp");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var uglify = require("gulp-uglify");
var browser = require("browser-sync");
var data = require('gulp-data');
var plumber = require("gulp-plumber");
var jade = require('gulp-jade');
//var path = require('path');

gulp.task("server", function() {
    browser({
        server: {
            baseDir: "./html"
        }
    });
});

gulp.task("sass", function() {
    gulp.src("src/assets/sass/**/*scss")
        .pipe(plumber())
        .pipe(sass({outputStyle: 'expanded'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest("html/assets/css"))
        .pipe(browser.reload({stream:true}));
});

gulp.task("img", function() {
    gulp.src("src/assets/img/**/*")
        .pipe(gulp.dest("html/assets/img"))
        .pipe(browser.reload({stream:true}));
});

gulp.task("js", function() {
    gulp.src(["src/assets/js/**/*.js","!src/assets/js/min/**/*.js"])
        .pipe(uglify())
        .pipe(gulp.dest("html/assets/js"))
        .pipe(browser.reload({stream:true}));
});

gulp.task('jade', function(){
    gulp.src(["src/**/*.jade","!src/templates/*.jade"])
    .pipe(data(function(file){
        return { 'meta': require('./src/meta.json')};
    }))
    .pipe(plumber())
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest("html"))
    .pipe(browser.reload({stream:true}));
});

gulp.task("html", function() {
    gulp.src(["html/**/*.html"])
        .pipe(browser.reload({stream:true}));
});

gulp.task("default",['server'], function() {
    gulp.src([
        'src/assets/img/**',
        'src/assets/js/**',
        'src/assets/sass/**'
    ], {
        base: 'src'
    })
    .pipe(gulp.dest('html'));
    gulp.watch("src/assets/sass/**/*.scss",["sass"]);
    gulp.watch("src/assets/img/**/*",["img"]);
    gulp.watch(["src/assets/js/**/*.js","!src/assets/js/min/**/*.js"],["js"]);
    gulp.watch(["src/**/*.jade", '!src/templates/*.jade'],["jade"]);
    gulp.watch("html/**/*.html",["html"]);
});